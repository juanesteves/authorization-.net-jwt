# Authorization .NET JWT

Project  on .NET, with JWT, role-based authorization and claims-based authorization.

## Getting started

The app is a small example of the role-based authorization method with .NET. In this, a login with JWT was created, the bearer header authorization was added to the swagger interface, and an administrator role was created.
If you want to verify with another role, in the authorization controller you can modify the role with which the token is created.
